module "ec2" {
  source = "./modules/ec2"
  subnet_id = module.network.public_subnet_id1
  security_group = module.network.security_group_id
  
}

module "network" {
  source = "./modules/network"
  
}