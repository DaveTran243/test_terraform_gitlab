output "public_subnet_id1" {
  value = aws_subnet.demosubnet.id
}
output "security_group_id" {
  value = aws_security_group.demosg.id
}