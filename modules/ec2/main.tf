# Creating key pair
resource "aws_key_pair" "demokey" {
  key_name   = "${var.key_name}"
  public_key = "${file(var.public_key)}"
}
  
# Creating EC2 Instance
resource "aws_instance" "demoinstance" {

  # AMI based on region 
  ami = "${lookup(var.ami, var.aws_region)}"

  # Launching instance into subnet 
  subnet_id = "${var.subnet_id}"
  

  # Instance type 
  instance_type = "${var.instancetype}"
  
  # Count of instance
  count= "${var.master_count}"
  
  # SSH key that we have generated above for connection
  key_name = "${aws_key_pair.demokey.id}"

  # Attaching security group to our instance
  vpc_security_group_ids = ["${var.security_group}"]

  # Attaching Tag to Instance 
  tags = { 
    Name = "DucTM41-${count.index + 1}"
  }
  # provisioner
  provisioner "local-exec" {
    command = "${file(var.setup_bash)}"
  }
  # Root Block Storage
  root_block_device {
    volume_size = "20"
    volume_type = "standard"
  }
  
  #EBS Block Storage
  ebs_block_device {
    device_name = "/dev/sdb"
    volume_size = "8"
    volume_type = "standard"
    delete_on_termination = false
  }
}
