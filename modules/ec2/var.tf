# Defining Public Key
variable "public_key" {
  default = "davekey1.pub"
}

# Definign Key Name for connection
variable "key_name" {
  default = "davekey1"
  description = "Desired name of AWS key pair"
}
# Defining AMI
variable "ami" {
  default = {
    eu-west-1 = "ami-0ea3405d2d2522162"
    us-east-1 = "ami-09d95fab7fff3776c"
  }
}

# Defining Instace Type
variable "instancetype" {
  default = "t3.micro"
}

# Defining Master count 
variable "master_count" {
  default = 1
}
# Defining Region
variable "aws_region" {
  default = "us-east-1"
}
variable "setup_bash" {
  default = "setup.sh"
}
variable "subnet_id" {}
variable "security_group" {}